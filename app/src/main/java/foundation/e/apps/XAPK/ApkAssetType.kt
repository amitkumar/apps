package foundation.e.apps.XAPK

enum class ApkAssetType(val suffix: String) {
    Apk(".apk"),
    XAPK(".xapk"),
    Apks(".apks")
}

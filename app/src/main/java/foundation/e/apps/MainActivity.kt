/*
    Copyright (C) 2019  e Foundation

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package foundation.e.apps

//import androidx.fragment.app.ListFragment
import android.annotation.SuppressLint
import android.content.SharedPreferences
import android.content.pm.PackageManager
import android.os.Bundle
import android.os.Handler
import android.preference.PreferenceManager
import android.view.MenuItem
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.google.android.material.bottomnavigation.BottomNavigationItemView
import com.google.android.material.bottomnavigation.BottomNavigationMenuView
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.google.android.material.bottomnavigation.LabelVisibilityMode
import com.google.android.material.snackbar.Snackbar
import foundation.e.apps.applicationmanager.ApplicationManager
import foundation.e.apps.applicationmanager.ApplicationManagerServiceConnection
import foundation.e.apps.applicationmanager.ApplicationManagerServiceConnectionCallback
import foundation.e.apps.categories.CategoriesFragment
import foundation.e.apps.home.HomeFragment
import foundation.e.apps.search.SearchFragment
import foundation.e.apps.settings.SettingsFragment
import foundation.e.apps.updates.UpdatesFragment
import foundation.e.apps.updates.UpdatesManager
import foundation.e.apps.utils.Constants
import foundation.e.apps.utils.Constants.CURRENTLY_SELECTED_FRAGMENT_KEY
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : AppCompatActivity(), BottomNavigationView.OnNavigationItemSelectedListener,
        ApplicationManagerServiceConnectionCallback {

    private var currentFragmentId = 0
    private val homeFragment = HomeFragment()
    private val searchFragment = SearchFragment()
    private val updatesFragment = UpdatesFragment()
    private val applicationManagerServiceConnection =
            ApplicationManagerServiceConnection(this)
    private val codeRequestPermissions = 9527
    var doubleBackToExitPressedOnce = false;



    companion object {
        lateinit var mActivity: MainActivity
        var sharedPreferences : SharedPreferences?=null
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        mActivity = this
        disableCategoryIfOpenSource()


        bottom_navigation_view.setOnNavigationItemSelectedListener{
            if (selectFragment(it.itemId,it)) {
                disableCategoryIfOpenSource()
                currentFragmentId = it.itemId
                return@setOnNavigationItemSelectedListener true
            }
            return@setOnNavigationItemSelectedListener false
        }

        disableShiftingOfNabBarItems()

        initialiseUpdatesWorker()


        // Show the home fragment by default
        currentFragmentId = if (savedInstanceState != null &&
                savedInstanceState.containsKey(CURRENTLY_SELECTED_FRAGMENT_KEY)) {
            savedInstanceState.getInt(CURRENTLY_SELECTED_FRAGMENT_KEY)
        } else if (intent.hasExtra(Constants.UPDATES_NOTIFICATION_CLICK_EXTRA)) {
            R.id.menu_updates
        } else {
            R.id.menu_home
        }

        applicationManagerServiceConnection.bindService(this)
    }

    private fun initialiseUpdatesWorker() {
        UpdatesManager(applicationContext).startWorker()


    }

    override fun onServiceBind(applicationManager: ApplicationManager) {
        initialiseFragments(applicationManager)
        selectFragment(currentFragmentId, null)
    }

    private fun initialiseFragments(applicationManager: ApplicationManager) {
        homeFragment.initialise(applicationManager)
        searchFragment.initialise(applicationManager)
        updatesFragment.initialise(applicationManager)
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        if (selectFragment(item.itemId,item)) {
            currentFragmentId = item.itemId
            return true
        }
        return false
    }

    fun showApplicationTypePreference(): String {
        val preferences = PreferenceManager.getDefaultSharedPreferences(mActivity)
        var showAllApps = preferences.getBoolean(mActivity.getString(R.string.Show_all_apps), true)
        var showAllOpenSourceApps = preferences.getBoolean(mActivity.getString(R.string.show_only_open_source_apps_key), false)
        var showAllPwaApps = preferences.getBoolean(mActivity.getString(R.string.show_only_pwa_apps_key), false)
        if (showAllOpenSourceApps) {
            return "open"
        } else if (showAllApps) {
            return "any"
        } else if (showAllPwaApps) {
            return "pwa"
        }
        return "any"
    }


    private fun selectFragment(fragmentId: Int, item: MenuItem?): Boolean {
        when (fragmentId) {
            R.id.menu_home -> {
                item?.setIcon(R.drawable.ic_menu_home)
                showFragment(homeFragment)
                return true
            }
            R.id.menu_categories -> {
                item?.setIcon(R.drawable.ic_menu_categories)
                showFragment(CategoriesFragment())
                return true
            }
            R.id.menu_search -> {
                item?.setIcon(R.drawable.ic_menu_search)
                showFragment(searchFragment)
                return true
            }
            R.id.menu_updates -> {
                item?.setIcon(R.drawable.ic_menu_updates)
                showFragment(updatesFragment)
                return true
            }
            R.id.menu_settings -> {
                item?.setIcon(R.drawable.ic_menu_settings)
                showFragment(SettingsFragment())
                return true
            }
        }
        return false
    }

    private fun showFragment(fragment: Fragment) {
        bottom_navigation_view.menu.findItem(currentFragmentId).isChecked = true
        supportFragmentManager
                .beginTransaction()
                .replace(R.id.frame_layout, fragment)
                .commit()
    }

    @SuppressLint("RestrictedApi")
    private fun disableShiftingOfNabBarItems() {
        val menuView = bottom_navigation_view.getChildAt(0) as BottomNavigationMenuView
        try {
            val mShiftingMode = menuView.javaClass.getDeclaredField("mShiftingMode")
            mShiftingMode.isAccessible = true
            mShiftingMode.setBoolean(menuView, false)
            mShiftingMode.isAccessible = false
        } catch (e: NoSuchFieldException) {
            e.printStackTrace()
        } catch (e: IllegalAccessException) {
            e.printStackTrace()
        }

        for (i in 0 until menuView.childCount) {
            val itemView = menuView.getChildAt(i) as BottomNavigationItemView
            itemView.setLabelVisibilityMode(LabelVisibilityMode.LABEL_VISIBILITY_LABELED);            itemView.setChecked(itemView.itemData.isChecked)
        }
    }

    private fun disableCategoryIfOpenSource(){
        if(showApplicationTypePreference()=="open") {
            bottom_navigation_view.menu.removeItem(R.id.menu_categories)
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>,
                                            grantResults: IntArray) {
        if (requestCode == Constants.STORAGE_PERMISSION_REQUEST_CODE &&
                grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_DENIED) {
            Snackbar.make(container, R.string.error_storage_permission_denied,
                    Snackbar.LENGTH_LONG).show()
        }
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState?.putInt(CURRENTLY_SELECTED_FRAGMENT_KEY, currentFragmentId)
    }

    override fun onDestroy() {
        super.onDestroy()
        homeFragment.decrementApplicationUses()
        searchFragment.decrementApplicationUses()
        updatesFragment.decrementApplicationUses()
        applicationManagerServiceConnection.unbindService(this)
    }

    override fun onBackPressed() {
        if (doubleBackToExitPressedOnce) {
            super.onBackPressed()
            return
        }

        this.doubleBackToExitPressedOnce = true;
        Toast.makeText(this, R.string.exit, Toast.LENGTH_SHORT).show();

        Handler().postDelayed(Runnable() {
            run {
                doubleBackToExitPressedOnce = false;
            }
        }, 2000)
    }
}